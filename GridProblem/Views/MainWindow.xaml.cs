﻿using System;
using System.Linq;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Data;
using Avalonia.Layout;
using Avalonia.Markup.Xaml;
using Avalonia.Media;

namespace GridProblem.Views
{
    public class MainWindow : Window
    {
        private readonly Grid _grid;

        public MainWindow()
        {
            InitializeComponent();

            _grid = this.FindControl<Grid>("grid");
        }

        private void OnRemoveRow(object sender, Avalonia.Interactivity.RoutedEventArgs e)
        {
            IControl lastChild = _grid.Children.Last();
            _grid.Children.Remove(lastChild);
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
