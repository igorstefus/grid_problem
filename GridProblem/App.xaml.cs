﻿using Avalonia;
using Avalonia.Markup.Xaml;

namespace GridProblem
{
    public class App : Application
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
